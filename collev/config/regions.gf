connect

create region --name=clienti --type=PARTITION_HEAP_LRU --redundant-copies=1
#create region --name=profiloCreditizio --type=PARTITION_HEAP_LRU --redundant-copies=1
create region --name=profiloCreditizio --type=PARTITION_OVERFLOW --disk-store=persistent_store --redundant_copies=1

create region --name=regionClientCabina --type=PARTITION_OVERFLOW --disk-store=persistent_store --redundant-copies=1
create region --name=regionBulkMetadata --type=PARTITION_OVERFLOW --disk-store=persistent_store --redundant-copies=1

define index --name=profiloCreditizioTiidClienteIndex --region="/profiloCreditizio o" --expression="o.id" --type="hash"
define index --name=clientiIdIndex --region="/clienti o" --expression="o.id" --type="hash"

